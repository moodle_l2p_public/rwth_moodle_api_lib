# RWTH Moodle Api

## Dependencies
- php7 with curl
- phive (php package manager)
	```bash
	cd /home/`whoami`/Downloads
	wget https://phar.io/releases/phive.phar
	wget https://phar.io/releases/phive.phar.asc
	gpg --keyserver hkps.pool.sks-keyservers.net --recv-keys 0x9B2D5D79
	gpg --verify phive.phar.asc phive.phar
	chmod +x phive.phar
	sudo mv phive.phar /usr/bin/phive
	```
- pear (php package manager) (maybe not needed)
	```bash
	wget http://pear.php.net/go-pear.phar
	sudo php go-pear.phar
	```
- phpunit (for unit tests)
	```bash
	sudo phive install -g phpunit
	# on php 7.0
	sudo phive install -g https://phar.phpunit.de/phpunit-6.5.5.phar
	```
- phpab (for generating an autoload file)
	```bash
	sudo phive install -g phpab
	```
- phpcs (syntax checking in vim)
	```bash
	sudo pear install PHP_CodeSniffer
	sudo phive install -g phpcs
	```
- phpcbf (correct syntax)
	```bash
	sudo phive install -g phpcbf
	```
- phpmd (mess detector)
	```bash
	composer global require phpmd/phpmd
	```

	- add ~/.config/composer/vendor/bin to your path
		- for example if you use bash add this to your ~/.bashrc
			```bash
			if [ -e $HOME/.config/composer/bin ]; then
				export PATH=${PATH}:$HOME/.config/composer/vendor/bin
			fi
			```
- other dependencies
	```bash
    composer install
    ```

## Coding Style
- PEAR
- Return Type should be on the line after the declaration (not checked by phpcs)
- Opening brace should be on the line after the declaration or the return type (not checked by phpcs)
- Private member variables must **not** be prefixed with an underscore
- variable names and member name should be camelCase except for pre/suffixes
- to check the style:
	- `phpcs --standard=ruleset.xml -n src`
	- `phpcs --standard=ruleset.xml -n tests`
- to correct the style automatically:
	- `phpcbf --standard=ruleset.xml -n src`
	- `phpcbf --standard=ruleset.xml -n tests`

## Mess Detector
- `phpmd src text mess_ruleset.xml`

## Contribution
Before contributing make sure:
- that the coding style is adhered
- that you created a new autoload file
- that all tests work

To do this, run this command:
```bash
./checkstyle.sh&&./gen_autoload.sh&&./run_tests.sh
```
