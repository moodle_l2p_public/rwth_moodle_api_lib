#!/bin/bash
# Generate PHP 5.2 compatible code
# phpab -c -o src/autoload.inc.php src

# Generate only the autoload file
phpab -o src/autoload.inc.php vendor/autoload.php src
