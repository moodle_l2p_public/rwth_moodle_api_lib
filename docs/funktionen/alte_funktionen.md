# Alte Funktionen, welche übernommen werden müssen
- Course
	- `viewAllCourseInfo`
	- `getCourseInfoRawData`
	- `viewCourseInfo`

- Announcement
	- `viewAllAnnouncements`

- Assignment
	- `viewAllAssignments`

- Email
	- `viewAllEmails`

- Hyperlink
	- `viewAllHyperlinks`

- LearningMaterial
	- `viewAllLearningMaterials`

- Literatur
	- `viewAllLiterature`

- SharedDocuments
	- `viewAllSharedDocuments`

- WhatsNew
	- `whatsNewSince`
	- `whatsAllNewSince`

- ExamResult
	- `viewExamResults`
	- `viewExamResultsStatistics`

- Download
	- `downloadFile/file`

- Special
	- `getSpecialAccessToken`
