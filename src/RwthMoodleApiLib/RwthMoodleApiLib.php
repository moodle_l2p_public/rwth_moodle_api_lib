<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  RwthMoodleApiLib
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib;

use RwthMoodleApiLib\RwthMoodleApiLibInterface;
use RwthMoodleApiLib\Logging\LoggingManagerInterface;
use RwthMoodleApiLib\Http as Http;
use RwthMoodleApiLib\Exceptions as Exceptions;

/**
 * RwthMoodleApiLib class contains all methods that are needed by the
 * Moodle Webservice API Wrapper
 *
 * @category  RwthMoodleApiLib
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
class RwthMoodleApiLib implements RwthMoodleApiLibInterface
{

	protected $loggingManager;           // error logging manager
	protected $httpClient;               // GuzzleHttp\Client Wrapper
	protected $moodleToken;              // token to execute webservices
	protected $webserviceUri;            // webservice/rest/server.php
	protected $webserviceLoginUri;       // login/token.php

	/**
	 * Constructor
	 *
	 * @param Logging\LoggingmanagerInterface $loggingManager Logging Manager
	 * @param Http\ClientInterface            $client         Http Client, should contain the moodle base url
	 * @param string                          $moodleToken    (optional) Already generated moodle token (default="")
	 *
	 * @return none
	 */
	public function __construct(Logging\LoggingManagerInterface $loggingManager, Http\ClientInterface $client, string $moodleToken="")
	{
		$this->loggingManager     = $loggingManager;
		$this->client             = $client;
		$this->moodleToken        = $moodleToken;
		$this->webserviceUri      = "webservice/rest/server.php";
		$this->webserviceLoginUri = "login/token.php";
	}

	/**
	 * Login and get the Moodle token
	 *
	 * @param string $username   Moodle username
	 * @param string $password   Moodle password
	 * @param string $webservice Moodle webservice to login
	 *
	 * @throws Exceptions\MoodleError
	 *
	 * @return bool true if login was successful
	 **/
	public function login(string $username, string $password, string $webservice)
		: bool
	{
		$params = array("username"=>$username, "password"=>$password, "service"=>$webservice);
		//call login URL
		$response = $this->client->request("POST", $this->webserviceLoginUri, ['form_params' => $params]);
		$login = json_decode($response->getBody());
		if (empty($login)) {
			return false;
		} else if (!empty($login->exception)) {
			throw new Exceptions\MoodleException($login->exception, $login->errorcode, $login->message, $response);
		} else if (!empty($login->error)) {
			throw new Exceptions\MoodleError($login->error, $login->errorcode, $response);
		} else if (empty($login->token) && empty($login->privatetoken)) {
			return false;
		}
		$this->moodleToken = $login->token??$login->privatetoken;
		return true;
	}

	/**
	 * Login and get the Moodle token with the ext_token_login plugin
	 *
	 * @param string $oauthToken        The external OAuth token
	 * @param string $extTokenLoginUser The username of the ext_token_login dummy user
	 * @param string $webservice        The webservice to login
	 *
	 * @throws Exceptions\MoodleError
	 *
	 * @return true if login was successful
	 **/
	public function extTokenLogin(string $oauthToken, string $extTokenLoginUser, string $webservice)
		: bool
	{
		$params = array("username"=>$extTokenLoginUser, "password"=>"", "token"=>$oauthToken, "service"=>$webservice);
		//call login URL
		$response = $this->client->request("POST", $this->webserviceLoginUri, ['form_params' => $params]);
		$login = json_decode($response->getBody());
		if (empty($login)) {
			return false;
		} else if (!empty($login->exception)) {
			throw new Exceptions\MoodleException($login->exception, $login->errorcode, $login->message, $response);
		} else if (!empty($login->error)) {
			if (Config::$debug_msgs) {
				print_r($login);
			}
			throw new Exceptions\MoodleError($login->error, $login->errorcode, $response);
		} else if (empty($login->token) && empty($login->privatetoken)) {
			return false;
		}
		$this->moodleToken = $login->token??$login->privatetoken;
		return true;
	}

	/**
	 * Perform a rest call to the (already logged in) moodle webservice
	 *
	 * @param string $function The function name to execute on the moode webservice
	 * @param array  $params   Parameters
	 *
	 * @throws Exception\NotLoggedInException
	 *
	 * @return Http\ResponseInterface The response from moodle
	 */
	public function restCall(string $function, array $params)
		: Http\ResponseInterface
	{
		if (empty($this->moodleToken)) {
			//TODO try to login automatically
			throw new Exceptions\NotLoggedInException("You need to be logged in to make a rest call");
		}
		$callurl = $this->webserviceUri."?wstoken=".$this->moodleToken."&wsfunction=$function&moodlewsrestformat=json";
		$response = $this->client->request("POST", $callurl, ['form_params' => $params]);
		$json = json_decode($response->getBody());
		if (!empty($json->exception)) {
			throw new Exceptions\MoodleException($json->exception, $json->errorcode, $json->message, $response);
		} else if (!empty($json->error)) {
			throw new Exceptions\MoodleError($json->error, $json->errorcode, $response);
		}
		return $response;
	}

	/**
	 * Get moodleToken.
	 *
	 * @return string moodleToken.
	 */
	public function getMoodleToken()
		: string
	{
		return $this->moodleToken;
	}

	/**
	 * Set moodleToken.
	 *
	 * @param string $moodleToken the value to set.
	 *
	 * @return none
	 */
	public function setMoodleToken(string $moodleToken)
	{
		$this->moodleToken = $moodleToken;
	}

	/**
	 * Get the base uri
	 *
	 * @return string base_uri
	 */
	public function getBaseUri()
		: string
	{
		return $this->client->getConfig("base_uri");
	}

}
?>
