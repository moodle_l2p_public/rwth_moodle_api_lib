<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  RwthMoodleApiLib
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Controller;

use RwthMoodleApiLib;
use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Http as Http;

/**
 * SharedDocumentInterface
 *
 * @category   Controller
 * @package    RwthMoodleApiLib
 * @subpackage Controller
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
interface SharedDocumentInterface
{

	/**
	 * Wrapper for l2p viewAllSharedDocuments
	 *
	 * Return all items from the Shared Documents module.
	 * This method will return all items from the Shared Documents modu
	 * le. From the returned items, parentFolderId parameter can be use
	 * d to build a folder tree of items in the module. Items with iden
	 * tical parentFolderId and itemId represent items in the root fold
	 * er. Metadata information about the item will be provided in file
	 * Information attribute of the returned item. A separate call to d
	 * ownloadFile API is necessary to download the item. The response
	 * contains a downloadUrl element which purpose is exclusively to b
	 * e used in the downloadFile method. The field selfUrl of the resp
	 * onse is returned for both folders and files, it allows to locate
	 * the resource in the file tree. Although sometimes the downloadUr
	 * l is the same as the selfUrl (files), the selfUrl does not work
	 * for the downloadFile method. Only authenticated users who are me
	 * mber of the specified courseroom can call this method and return
	 * ed items will be dependant on authenticated user's permission.
	 *
	 * @param string $cid Desired courseroom id
	 *
	 * @return viewAllSharedDocuments
	 */
	public function viewAllSharedDocuments(string $cid);

}
?>
