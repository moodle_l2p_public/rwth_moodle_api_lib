<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  RwthMoodleApiLib
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Controller;

use RwthMoodleApiLib;
use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Http as Http;

/**
 * CourseRoom Controller
 *
 * @category   Controller
 * @package    RwthMoodleApiLib
 * @subpackage Controller
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
class CourseRoom extends ApiController implements CourseRoomInterface
{

	public static $COURSE_STATUS = array("open", "closed", "readonly", "readonly+closed", "downgraded", "unknown");

	/**
	 * Constructor
	 *
	 * @param RwthMoodleApiLib\RwthMoodleApiLibInterface $api RwthMoodleApiLib
	 */
	public function __construct(RwthMoodleApiLib\RwthMoodleApiLibInterface $api)
	{
		parent::__construct($api);
	}

	/**
	 * Wrapper for l2p viewAllCourseInfo
	 *
	 * Get all course information.
	 * This method returns all course information from L2P. The CourseS
	 * tatus can be either  open, closed, reaadonly, readonly+closed, d
	 * owngraded or unknown.  Both authenticated user and annonymously
	 * authenticated user can call this method and returned dataset wil
	 * l be same for all.
	 *
	 * @return viewAllCourseInfo
	 */
	public function viewAllCourseInfo()
	{
		$courses_raw = $this->getApi()->restCall('core_course_get_courses', array());
		$courses = json_decode($courses_raw->getBody());
		$dataSet = array();
		foreach ($courses as $course) {
			//TODO
			/* $closed = !(((int)$course->enddate)===0 || ((int)$course->enddate)>time()); */

			$uniqueid = $course->id; // "12ws-00000",
			$semester = "TODO"; // "ws12",
			$courseTitle = $course->displayname; // "Introduction to API",
			$courseTitle = $course->fullname; // "Introduction to API",
			$description = $course->summary; // "A sample courseroom for sandbox usage.",
			$url = $this->getApi()->getBaseUri() . "course/view.php?id=" . $course->id; // "https=>//www3.elearning.rwth-aachen.de/ws12/12ws-00000",
			$courseStatus = "TODO"; // "open",
			$itemId = "TODO"; // 29,
			$Status = "TODO"; // true
			$dataSet[] = array(
				"uniqueid"=> $uniqueid, // "12ws-00000",
				"semester"=> $semester, // "ws12",
				"courseTitle"=> $courseTitle, // "Introduction to API",
				"description"=> $description, // "A sample courseroom for sandbox usage.",
				"url"=> $url, // "https=>//www3.elearning.rwth-aachen.de/ws12/12ws-00000",
				"courseStatus"=> $courseStatus, // "open",
				"itemId"=> $itemId, // 29,
				"Status"=> $Status // true
			);
		}
		$Status = "TODO"; // true
		$out = array(
			"dataSet"=> $dataSet,
			"Status"=> $Status // true
		);
		return $out;
	}

	/**
	 * Wrapper for l2p getCourseInfoRawData
	 *
	 * @return getCourseInfoRawData
	 */
	public function getCourseInfoRawData()
	{
		echo "This method is not implemented yet!";
		return null;
	}

	/**
	 * Wrapper for l2p viewCourseInfo
	 *
	 * Get information about a course.
	 * This method returns information about a course. The CourseStatus
	 * can be either  open, closed, reaadonly, readonly+closed, downgra
	 * ded or unknown.  Both authenticated user and annonymously authen
	 * ticated user can call this method and returned dataset will be s
	 * ame for all.
	 *
	 * @param string $cid Desired courseroom id
	 *
	 * @return viewCourseInfo
	 */
	public function viewCourseInfo(string $cid)
	{
		$params = array('options'=>array('ids'=>array($cid)));
		$courses_raw = $this->getApi()->restCall('core_course_get_courses', $params);
		$courses = json_decode($courses_raw->getBody());
		$dataSet = array();
		foreach ($courses as $course) {
			$uniqueid = $course->id; // "12ws-00000",
			$semester = "TODO"; // "ws12",
			$courseTitle = $course->displayname; // "Introduction to API",
			$courseTitle = $course->fullname; // "Introduction to API",
			$description = $course->summary; // "A sample courseroom for sandbox usage.",
			$url = $this->getApi()->getBaseUri() . "course/view.php?id=" . $course->id; // "https=>//www3.elearning.rwth-aachen.de/ws12/12ws-00000",
			$courseStatus = "TODO"; // "open",
			$itemId = "TODO"; // 29,
			$Status = "TODO"; // true
			$dataSet[] = array(
				"uniqueid"=> $uniqueid, // "12ws-00000",
				"semester"=> $semester, // "ws12",
				"courseTitle"=> $courseTitle, // "Introduction to API",
				"description"=> $description, // "A sample courseroom for sandbox usage.",
				"url"=> $url, // "https=>//www3.elearning.rwth-aachen.de/ws12/12ws-00000",
				"courseStatus"=> $courseStatus, // "open",
				"itemId"=> $itemId, // 29,
				"Status"=> $Status // true
			);
		}
		$Status = "TODO"; // true
		$out = array(
			"dataSet"=> $dataSet,
			"Status"=> $Status // true
		);
		return $out;
	}

}

?>
