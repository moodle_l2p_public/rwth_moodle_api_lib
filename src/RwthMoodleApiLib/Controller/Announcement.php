<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  RwthMoodleApiLib
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Controller;

use RwthMoodleApiLib;
use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Http as Http;

/**
 * Announcement
 *
 * @category   Controller
 * @package    RwthMoodleApiLib
 * @subpackage Controller
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
class Announcement extends CourseRoom implements AnnouncementInterface
{

	/**
	 * Constructor
	 *
	 * @param RwthMoodleApiLib\RwthMoodleApiLibInterface $api RwthMoodleApiLib
	 */
	public function __construct(RwthMoodleApiLib\RwthMoodleApiLib $api)
	{
		parent::__construct($api);
	}

	/**
	 * Wrapper for l2p viewAllAnnouncements
	 *
	 * Return all announcements from the Announcement module.
	 * This method will return all announcements from the Announcement
	 * module. Expired and non-expired announcents both will be returne
	 * d from the list and items with null expirytime will be reported
	 * as  "expireTime": 0 . If an announcement item contains attacheme
	 * nts then metadata about the attachement(s) will be provided. A s
	 * eparate call to downloadFile API is necessary to download the at
	 * tachment(s). The response contains a downloadUrl element which p
	 * urpose is exclusively to be used in the downloadFile method. Onl
	 * y authenticated users who are member of the specified courseroom
	 * can call this method and returned items will be dependant on aut
	 * henticated user's permission.
	 *
	 * @param string $cid Desired courseroom id
	 *
	 * @return viewAllAnnouncements
	 */
	public function viewAllAnnouncements(string $cid)
	{
		//core_course_get_contents returnt eine liste mit contents(die topics)
		//das element 0 enthält die obersten, welche keinen topic namen haben
		//in [0] gibt es eine list mit modules und in einem der modules ist ein object
		//mit dem namen Announcements und dem modname forum. bis jetzt ist es immer das
		//1. element der liste gewesen, das sollte man sich aber nochmal genau anschauen
		//TODO genau anschauen
		$params_content = array('courseid'=>$cid);
		$course_content_raw = $this->getApi()->restCall('core_course_get_contents', $params_content);
		$course_content = json_decode($course_content_raw->getBody());
		//in dem module ist jetzt die forum id als 'instance' gespeichert, darüber
		//können wir die announcements abrufen
		$forumid = $course_content[0]->modules[0]->instance;
		$params_forum = array('forumid'=>$forumid);
		$announcements_raw = $this->getApi()->restCall('mod_forum_get_forum_discussions_paginated', $params_forum);
		$announcements = json_decode($announcements_raw->getBody());
		$dataSet = array();
		foreach ($announcements->discussions as $announcement) {
			$attachments = array();
			$modifiedTimestamp = $announcement->timemodified; // 123456798,
			$created = $announcement->created; // 123456798,
			$itemId = $announcement->id; // 29,
			$attachmentDirectory = "TODO"; // "/ss14/14ss-0000/Lists/AnnouncementDocuments/",
			$title = $announcement->name; // "This is a sample announcement.",
			$body = $announcement->message; // "Here goes the description of the announcement",
			//TODO muss vllt noch umgerechnet werden
			$expireTime = $announcement->timeend; // 123456789
			$attachments = array();
			if ($announcement->attachment) {
				foreach ($announcement->attachments as $attachment) {
					$downloadUrl = $attachment->fileurl; // "collaboration|/ws13/13ws-00001/Lists/wikiList1/1_.000",
					$itemId = "TODO"; // 29,
					$fileSize = $attachment->filesize; // "44",
					$modifiedTimestamp = $attachment->timemodified; // 987654321,
					$fileName = $attachment->filename; // "readme.txt"
					$attachment = array(
						"downloadUrl"=> $downloadUrl, // "collaboration|/ws13/13ws-00001/Lists/wikiList1/1_.000",
						"itemId"=> $itemId, // 29,
						"fileSize"=> $fileSize, // "44",
						"modifiedTimestamp"=> $modifiedTimestamp, // 987654321,
						"fileName"=> $fileName // "readme.txt"
					);
					$attachments[] = $attachment;
				}
			}
			$dataSet[] = array(
				"attachments"=> $attachments,
				"modifiedTimestamp"=> $modifiedTimestamp, // 123456798,
				"created"=> $created, // 123456798,
				"itemId"=> $itemId, // 29,
				"attachmentDirectory"=> $attachmentDirectory, // "/ss14/14ss-0000/Lists/AnnouncementDocuments/",
				"title"=> $title, // "This is a sample announcement.",
				"body"=> $body, // "Here goes the description of the announcement",
				"expireTime"=> $expireTime // 123456789
			);
		}
		$Status = "TODO"; // true
		$out = array(
			"dataSet"=> $dataSet,
			"Status"=> $Status // true
		);
		return $out;
	}

}
?>
