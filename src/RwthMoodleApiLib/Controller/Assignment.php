<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  RwthMoodleApiLib
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Controller;

use RwthMoodleApiLib;
use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Http as Http;

/**
 * Assignment
 *
 * @category   Controller
 * @package    RwthMoodleApiLib
 * @subpackage Controller
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
class Assignment extends CourseRoom implements AssignmentInterface
{

	/**
	 * Constructor
	 *
	 * @param RwthMoodleApiLib\RwthMoodleApiLibInterface $api RwthMoodleApiLib
	 */
	public function __construct(RwthMoodleApiLib\RwthMoodleApiLib $api)
	{
		parent::__construct($api);
	}

	/**
	 * Wrapper for l2p viewAllAssignments
	 *
	 * Return all assignments from the Assignments module.
	 * This method will return all assignments from the Assignment modu
	 * le. If the assignment has associated documents, then A separate
	 * call to downloadFile API is necessary to download the assignment
	 * documents. The response contains a downloadUrl element which pur
	 * pose is exclusively to be used in the downloadFile method. Only
	 * authenticated users who are member of the specified courseroom c
	 * an call this method and returned items will be dependant on auth
	 * enticated user's permission.
	 *
	 * @param string $cid Desired courseroom id
	 *
	 * @return viewAllAssignments
	 */
	public function viewAllAssignments(string $cid)
	{
		$cid;
		return;
	}

}
?>
