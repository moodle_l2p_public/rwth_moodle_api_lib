<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  RwthMoodleApiLib
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Controller;

use RwthMoodleApiLib;
use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Http as Http;

/**
 * WhatsNew
 *
 * @category   Controller
 * @package    RwthMoodleApiLib
 * @subpackage Controller
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
class WhatsNew extends CourseRoom implements WhatsNewInterface
{

	/**
	 * Constructor
	 *
	 * @param RwthMoodleApiLib\RwthMoodleApiLibInterface $api RwthMoodleApiLib
	 */
	public function __construct(RwthMoodleApiLib\RwthMoodleApiLib $api)
	{
		parent::__construct($api);
	}

	/**
	 * Wrapper for l2p whatsNewSince
	 *
	 * Get new items with definable timespan.
	 * This method will return the items that were modified in the last
	 * pastMinutes minutes. Only positive values of pastMinutes are all
	 * owed. Only authenticated users who are member of the specified c
	 * ourseroom can call this method and returned items will be depend
	 * ant on authenticated user's permission.
	 *
	 * @param string $cid         Desired courseroom id
	 * @param int    $pastMinutes pastMinutes
	 *
	 * @return whatsNewSince
	 */
	public function whatsNewSince(string $cid, int $pastMinutes)
	{
		$since = time()-($pastMinutes*60);
		$params = array('courseid'=>$cid, 'since'=>$since, 'filter'=>array());
		/* $params = array('options'=>array('courseid'=>$cid, 'since'=>$since, 'filter'=>array())); */
		$updates_raw;
		try {
			$updates_raw = $this->getApi()->restCall('core_course_get_updates_since', $params);
			$updates = json_decode($updates_raw->getBody());
			/* print_r($updates); */
			$announcements = array();
			$assignments = array();
			$discussionItems = array();
			$emails = array();
			$literature = array();
			$learningMaterials = array();
			$mediaLibraries = array();
			$sharedDocuments = array();
			$wikis = array();
			foreach ($updates->instances as $update) {
				/* print_r($update); */
				/* $params_mod = array("cmid"=>$update->id); */
				/* print_r($params_mod); */
				/* $module_raw = $this->getApi()->restCall('core_course_get_course_module', $params_mod); */
			}
			$Status = "TODO"; // true
			$out = array(
				"announcements"=> $announcements,
				"assignments"=> $assignments,
				"discussionItems"=> $discussionItems,
				"emails"=> $emails,
				"literature"=> $literature,
				"learningMaterials"=> $learningMaterials,
				"mediaLibraries"=> $mediaLibraries,
				"sharedDocuments"=> $sharedDocuments,
				"wikis"=> $wikis,
				"Status"=> $Status // true
			);
			/* $module_raw = $this->getApi()->restCall('mod_forum_get_forum_discussions_paginated', array("forumid"=>18)); */
			/* $module_raw = $this->getApi()->restCall('core_course_get_course_module_by_instance', array("module"=>"module", "instance"=>18)); */
			/* $module_raw = $this->getApi()->restCall('core_course_get_course_module', array("cmid"=>18)); */
			/* print_r(json_decode($module_raw->getBody())); */
			/* echo "----------"; */
			/* $module_raw = $this->getApi()->restCall('core_course_get_course_module', array("cmid"=>333)); */
			/* print_r($module_raw); */
			/* $module_raw = $this->getApi()->restCall('core_course_get_course_module', array("cmid"=>334)); */
			/* print_r($module_raw); */
		} catch (\RwthMoodleApiLib\Exceptions\MoodleException $m){
			echo "FEHHHLER";
			echo (string)$m;
		}
		return $out;
	}

	/**
	 * Wrapper for l2p whatsAllNewSince
	 *
	 * Get new items of all your Courserooms with definable timespan.
	 * This method will return the items that were modified in the last
	 * pastMinutes minutes in all course rooms. Only positive values of
	 * pastMinutes are allowed. Remark: Only the courses of the current
	 * Semester will be queried. Only authenticated users who are membe
	 * r of the specified courseroom can call this method and returned
	 * items will be dependant on authenticated user's permission.
	 *
	 * @param int $pastMinutes pastMinutes
	 *
	 * @return whatsAllNewSince
	 */
	public function whatsAllNewSince(int $pastMinutes)
	{
	}

}
?>
