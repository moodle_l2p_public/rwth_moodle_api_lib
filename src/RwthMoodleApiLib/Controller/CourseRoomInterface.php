<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  RwthMoodleApiLib
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Controller;

use RwthMoodleApiLib;
use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Http as Http;

/**
 * CourseRoom Controller Interface
 *
 * @category   Controller
 * @package    RwthMoodleApiLib
 * @subpackage Controller
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
interface CourseRoomInterface
{

	/**
	 * Wrapper for l2p viewAllCourseInfo
	 *
	 * Get all course information.
	 * This method returns all course information from L2P. The CourseS
	 * tatus can be either  open, closed, reaadonly, readonly+closed, d
	 * owngraded or unknown.  Both authenticated user and annonymously
	 * authenticated user can call this method and returned dataset wil
	 * l be same for all.
	 *
	 * @return viewAllCourseInfo
	 */
	public function viewAllCourseInfo();

	/**
	 * Wrapper for l2p getCourseInfoRawData
	 *
	 * @return getCourseInfoRawData
	 */
	public function getCourseInfoRawData();

	/**
	 * Wrapper for l2p viewCourseInfo
	 *
	 * Get information about a course.
	 * This method returns information about a course. The CourseStatus
	 * can be either  open, closed, reaadonly, readonly+closed, downgra
	 * ded or unknown.  Both authenticated user and annonymously authen
	 * ticated user can call this method and returned dataset will be s
	 * ame for all.
	 *
	 * @param string $cid Desired courseroom id
	 *
	 * @return viewCourseInfo
	 */
	public function viewCourseInfo(string $cid);

}
?>
