<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  RwthMoodleApiLib
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Controller;

use RwthMoodleApiLib;
use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Http as Http;

/**
 * StructuredMaterial
 *
 * @category   Controller
 * @package    RwthMoodleApiLib
 * @subpackage Controller
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
class LearningMaterial extends CourseRoom implements LearningMaterialInterface
{

	/**
	 * Constructor
	 *
	 * @param RwthMoodleApiLib\RwthMoodleApiLibInterface $api RwthMoodleApiLib
	 */
	public function __construct(RwthMoodleApiLib\RwthMoodleApiLib $api)
	{
		parent::__construct($api);
	}

	/**
	 * Wrapper for l2p viewAllLearningMaterials
	 *
	 * Return all learning material items from the Learning Materials module.
	 * This method will return all learning material items from the Lea
	 * rning Material module. The items are accesible in at least one o
	 * f the available views (Default View| Topic Matrix| All Files| Da
	 * te Matrix). From the returned items, parentFolderId parameter ca
	 * n be used to build a folder tree of items in the module. Items w
	 * ith identical parentFolderId and itemId represent items in the r
	 * oot folder. Metadata information about the item will be provided
	 * in fileInformation attribute of the returned item. A separate ca
	 * ll to downloadFile API is necessary to download the item. The re
	 * sponse contains a downloadUrl element which purpose is exclusive
	 * ly to be used in the downloadFile method. The field selfUrl of t
	 * he response is returned for both folders and files, it allows to
	 * locate the resource in the file tree. Although sometimes the dow
	 * nloadUrl is the same as the selfUrl (files), the selfUrl does no
	 * t work for the downloadFile method. Only authenticated users who
	 * are member of the specified courseroom can call this method and
	 * returned items will be dependant on authenticated user's permiss
	 * ion.
	 *
	 * @param string $cid Desired courseroom id
	 *
	 * @return viewAllLearningMaterials
	 */
	public function viewAllLearningMaterials(string $cid)
	{
		$cid;
		return;
	}

}
?>
