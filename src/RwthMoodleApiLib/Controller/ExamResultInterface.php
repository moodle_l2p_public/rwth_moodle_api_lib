<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  RwthMoodleApiLib
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Controller;

use RwthMoodleApiLib;
use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Http as Http;

/**
 * ExamIResultnterface
 *
 * @category   Controller
 * @package    RwthMoodleApiLib
 * @subpackage Controller
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
interface ExamResultInterface
{

	/**
	 * Wrapper for l2p viewExamResults
	 *
	 * View Exam Results.
	 * View the exam results for a course room. If the user is manager,
	 * all items will be returned. Otherwise, only the item belonging too
	 * the user is returned if it is published by the managers. The
	 * custom fields will be returned as a List of Key/Value-Pairs
	 * (string/string) because these fields will vary depending on course
	 * and user. Only authenticated users who are member of the specified
	 * courseroom can call this method and returned items will be dependant
	 * on authenticated user's permission.
	 *
	 * @param string $cid Desired courseroom id
	 *
	 * @return viewExamResults
	 */
	public function viewExamResults(string $cid);

	/**
	 * Wrapper for l2p viewExamResultsStatistics
	 *
	 * View Grade Distribution.
	 * View the Grade Distribution of exam results for a course room. I
	 * f the grades are not published yet, an empty set of grades is re
	 * turned. The call will only provide statistics about published gr
	 * ades. Only authenticated users who are member of the specified c
	 * ourseroom can call this method and returned items will be depend
	 * ant on authenticated user's permission.
	 *
	 * @param string $cid Desired courseroom id
	 *
	 * @return viewExamResultsStatistics
	 */
	public function viewExamResultsStatistics(string $cid);

}
?>
