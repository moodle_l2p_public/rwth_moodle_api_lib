<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   Logging
 * @package    RwthMoodleApiLib
 * @subpackage Logging
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Logging;

use RwthMoodleApiLib\Logging\ObserverInterface;

/**
 * Abstract observer logging class
 *
 * @category   Logging
 * @package    RwthMoodleApiLib
 * @subpackage Logging
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
abstract class Observer implements ObserverInterface
{

	protected $severity;

	/**
	 * Constructor
	 *
	 * @param int $severity The severity level
	 **/
	public function __construct(int $severity)
	{
		$this->severity = $severity;
	}

	/**
	 * Returns the severity level on which this observer should be called
	 *
	 * Each message Priority also has a decimal Severity level indicator.
	 * These are described in the following table along with their numerical
	 * values. Severity values MUST be in the range of 0 to 7 inclusive.
	 * The list of severities is also defined by RFC 5424 (https://tools.ietf.org/html/rfc5424)
	 *
	 * Code | Severity
	 * -----|---------------
	 *  0   | Emergency: system is unusable
	 *  1   | Alert: action must be taken immediately
	 *  2   | Critical: critical conditions
	 *  3   | Error: error conditions
	 *  4   | Warning: warning conditions
	 *  5   | Notice: normal but significant condition
	 *  6   | Informational: informational messages
	 *  7   | Debug: debug-level messages
	 *
	 * @return int
	 **/
	public function getSeverityLvl()
		: int
	{
		return $this->severity;
	}

	/**
	 * Sets the severity level on which this observer should be called
	 *
	 * @param int $severity Severity level
	 *
	 * @return none
	 **/
	public function setSeverityLvl(int $severity)
	{
		$this->severity = $severity;
	}

	/**
	 * Returns the severity as string
	 *
	 * @return string
	 **/
	public function getSeverityString()
	{
		switch ($this->severity)
		{
		case 0:
			return "emerg";
		case 1:
			return "alert";
		case 2:
			return "crit";
		case 3:
			return "err";
		case 4:
			return "warning";
		case 5:
			return "notice";
		case 6:
			return "info";
		case 7:
			return "debug";
		default:
			return "invalid severity";
		}
	}

	/**
	 * Returns the description of the current severity level
	 *
	 * @return string
	 **/
	public function getSeverityDescription()
	{
		switch ($this->severity)
		{
		case 0:
			return "Emergency: system is unusable";
		case 1:
			return "Alert: action must be taken immediately";
		case 2:
			return "Critical: critical conditions";
		case 3:
			return "Error: error conditions";
		case 4:
			return "Warning: warning conditions";
		case 5:
			return "Notice: normal but significant condition";
		case 6:
			return "Informational: informational messages";
		case 7:
			return "Debug: debug-level messages";
		default:
			return "Invalid severity: No description";
		}
	}

}

?>
