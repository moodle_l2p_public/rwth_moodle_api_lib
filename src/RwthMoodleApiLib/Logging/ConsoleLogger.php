<?php
/**
 * The Moodle Webservice Api Wrapper of the Rwth Aachen University
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   Logging
 * @package    RwthMoodleApiLib
 * @subpackage Logging
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Logging;

use RwthMoodleApiLib\Logging\ObserverInterface;
use RwthMoodleApiLib\Logging\Observer;

/**
 * Observer that writes everything to the console
 *
 * @category   Logging
 * @package    RwthMoodleApiLib
 * @subpackage Logging
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
class ConsoleLogger extends Observer implements ObserverInterface
{

	/**
	 * Constructor for ConsoleLogger
	 *
	 * @param int $severity (optional) severity level (default=4;warning)
	 **/
	public function __construct(int $severity=4)
	{
		parent::__construct($severity);
	}

	/**
	 * Writes messages to the console
	 *
	 * @param string $argument argument to write to the console
	 * @param int    $severity severity
	 *
	 * @return none
	 **/
	public function update(string $argument, int $severity=5)
	{
		echo "\n-------------\n";
		echo "$argument\n";
	}

	/**
	 * Writes an error report to the console
	 *
	 * @param int    $errorCode    the error code
	 * @param string $errorMessage the error message
	 * @param any    $subject      on this object the error appeared
	 * @param int    $severity     severity
	 *
	 * @return none
	 **/
	public function reportError(int $errorCode, string $errorMessage, $subject, int $severity=3)
	{
		echo "ERROR:\nError Code: \t$errorCode\nError Message: \t$errorMessage";
	}

}

?>
