<?php
/**
 * The Moodle Webservice Api Wrapper of the Rwth Aachen University
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   Logging
 * @package    RwthMoodleApiLib
 * @subpackage Logging
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Logging;

/**
 * Observers are for logging messages
 *
 * @category   Logging
 * @package    RwthMoodleApiLib
 * @subpackage Logging
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
interface ObserverInterface
{

	/**
	 * Writes a message
	 *
	 * @param string $argument message to write
	 * @param int    $severity severity
	 *
	 * @return none
	 **/
	public function update(string $argument, int $severity);

	/**
	 * Writes an ErrorReport
	 *
	 * @param int    $errorCode    the error code
	 * @param string $errorMessage the error message
	 * @param any    $subject      on this object the error appeared
	 * @param int    $severity     severity
	 *
	 * @return none
	 **/
	public function reportError(int $errorCode, string $errorMessage, $subject, int $severity);

	/**
	 * Returns the severity level on which this observer should be called
	 *
	 * Each message Priority also has a decimal Severity level indicator.
	 * These are described in the following table along with their numerical
	 * values. Severity values MUST be in the range of 0 to 7 inclusive.
	 * The list of severities is also defined by RFC 5424 (https://tools.ietf.org/html/rfc5424)
	 *
	 * Code | Severity
	 * -----|---------------
	 *  0   | Emergency: system is unusable
	 *  1   | Alert: action must be taken immediately
	 *  2   | Critical: critical conditions
	 *  3   | Error: error conditions
	 *  4   | Warning: warning conditions
	 *  5   | Notice: normal but significant condition
	 *  6   | Informational: informational messages
	 *  7   | Debug: debug-level messages
	 *
	 * @return int
	 **/
	public function getSeverityLvl()
		: int;

}
?>
