<?php
/**
 * The Moodle Webservice Api Wrapper of the Rwth Aachen University
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   Logging
 * @package    RwthMoodleApiLib
 * @subpackage Logging
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Logging;

use RwthMoodleApiLib\Logging\LoggingManagerInterface;
use RwthMoodleApiLib\Logging\ObserverInterface;
use RwthMoodleApiLib\Logging\Observer;

/**
 * LoggingController
 *
 * @category   Logging
 * @package    RwthMoodleApiLib
 * @subpackage Logging
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
class LoggingManager implements LoggingManagerInterface
{

	protected $observers;

	/**
	 * Constructor
	 **/
	public function __construct()
	{
		$this->observers = [];
	}

	/**
	 * Adds an observer
	 *
	 * @param Observer $observer the observer to add
	 *
	 * @return none
	 **/
	public function addObserver(ObserverInterface $observer)
	{
		$this->observers[] = $observer;
	}

	/**
	 * Returns all added observers
	 *
	 * @return array
	 **/
	public function getObservers()
		: array
	{
		return $this->observers;
	}

	/**
	 * Notifies all observers to update this argument
	 *
	 * If a severity level is given, it ignores messages with lower severity level
	 *
	 * @param string $argument argument to update
	 * @param int    $severity (optional) severity level (default=5;notice)
	 *
	 * @return none
	 **/
	public function notifyObservers(string $argument, int $severity=5)
	{
		foreach ($this->observers as $observer) {
			if ($observer->getSeverityLvl() >= $severity) {
				$observer->update($argument, $severity);
			}
		}
	}

	/**
	 * Reports an error to all observers
	 *
	 * The subject should be the $this from where the method is called
	 *
	 * @param int    $errorCode    the error code
	 * @param string $errorMessage the error message
	 * @param any    $subject      the subject of the error
	 * @param int    $severity     (optional) severity level (default=3;error)
	 *
	 * @return none
	 **/
	public function reportError(int $errorCode, string $errorMessage, $subject, int $severity=3)
	{
		foreach ($this->observers as $observer) {
			if ($observer->getSeverityLvl() >= $severity) {
				$observer->reportError($errorCode, $errorMessage, $subject, $severity);
			}
		}
	}

}

?>
