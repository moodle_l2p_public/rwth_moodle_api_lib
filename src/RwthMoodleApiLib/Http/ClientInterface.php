<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   Http
 * @package    RwthMoodleApiLib
 * @subpackage Http
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Http;

/**
 * Http client wrapper interface of Psr\Http\Message\MessageInterface
 *
 * @category   Http
 * @package    RwthMoodleApiLib
 * @subpackage Http
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
interface ClientInterface
{

	/**
	 * Create and send an HTTP request.
	 *
	 * Use an absolute path to override the base path of the client, or a
	 * relative path to append to the base path of the client. The URL can
	 * contain the query string as well.
	 *
	 * @param string              $method  HTTP method.
	 * @param string|UriInterface $uri     URI object or string.
	 * @param array               $options Request options to apply.
	 *
	 * @return ResponseInterface
	 * @throws HttpException
	 */
	public function request($method, $uri, array $options = [])
		: ResponseInterface;

	/**
	 * Get a client configuration option.
	 *
	 * These options include default request options of the client, a "handler"
	 * (if utilized by the concrete client), and a "base_uri" if utilized by
	 * the concrete client.
	 *
	 * @param string|null $option The config option to retrieve.
	 *
	 * @return mixed
	 */
	public function getConfig($option = null);

}
?>
