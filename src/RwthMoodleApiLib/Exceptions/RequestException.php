<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   Exceptions
 * @package    RwthMoodleApiLib
 * @subpackage Exceptions
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Exceptions;

use RwthMoodleApiLib\Http\RequestInterface;
use RwthMoodleApiLib\Http\ResponseInterface;

/**
 * Http Request Exception
 *
 * @category   Exceptions
 * @package    RwthMoodleApiLib
 * @subpackage Exceptions
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
class RequestException extends HttpException implements RequestExceptionInterface
{

	protected $requestException;

	/**
	 * Constructor
	 *
	 * @param \GuzzleHttp\Exception\RequestInterface $gzhttp RequestException to wrap
	 **/
	public function __construct(\GuzzleHttp\Exception\RequestException $gzhttp)
	{
		parent::__construct($gzhttp);
		$this->requestException = $gzhttp;
	}

	/**
	 * Get the request that caused the exception
	 *
	 * @return RequestInterface
	 */
	public function getRequest()
		: RequestInterface
	{
		return $this->requestException->getRequest();
	}

	/**
	 * Get the associated response
	 *
	 * @return ResponseInterface|null
	 */
	public function getResponse()
		: ResponseInterface
	{
		return $this->requestException->getResponse();
	}

}
?>
