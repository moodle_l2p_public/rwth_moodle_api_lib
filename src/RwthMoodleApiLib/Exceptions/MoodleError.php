<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   Exceptions
 * @package    RwthMoodleApiLib
 * @subpackage Exceptions
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Exceptions;

use RwthMoodleApiLib\Http as Http;

/**
 * Exception for Moodle Errors
 *
 * @category   Exceptions
 * @package    RwthMoodleApiLib
 * @subpackage Exceptions
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
class MoodleError extends MoodleResponseException implements MoodleErrorInterface
{

	protected $error;
	protected $errorCode;

	/**
	 * Constructor
	 *
	 * @param string                 $error     The Error returned from moodle
	 * @param string                 $errorCode The ErrorCode returned from moodle
	 * @param Http\ResponseInterface $response  The response returned from moodle
	 * @param Http\RequestInterface  $request   (optional) The request send to moodle (default=null)
	 */
	public function __construct(string $error, string $errorCode, Http\ResponseInterface $response, Http\RequestInterface $request=null)
	{
		parent::__construct($response, $request);
		$this->error     = $error;
		$this->errorCode = $errorCode;
	}

	/**
	 * Returns the error received from moodle.
	 *
	 * In general this is a short description.
	 *
	 * @return string
	 */
	public function getError()
		: string
	{
		return $this->error;
	}

	/**
	 * Returns the error code received from moodle.
	 *
	 * @return string
	 */
	public function getErrorCode()
		: string
	{
		return $this->errorCode;
	}


}
?>
