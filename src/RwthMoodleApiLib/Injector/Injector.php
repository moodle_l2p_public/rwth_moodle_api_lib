<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  RwthMoodleApiLib
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Injector;

use RwthMoodleApiLib;
use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Http as Http;

/**
 * Creates all dependent objects and injects them into RwthMoodleApiLib class
 *
 * @category   Injector
 * @package    RwthMoodleApiLib
 * @subpackage Injector
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
class Injector implements InjectorInterface
{

	protected $observers;
	protected $loggingManager;
	protected $httpClient;
	protected $rwthMoodleApiLib;

	/**
	 * Constructor
	 *
	 * @param string $url The url to moodle
	 **/
	public function __construct($url=null)
	{
		//observers
		$infoObserver = new Logging\ConsoleLogger(6);
		$warningObserver = new Logging\ConsoleLogger(4);
		$criticalObserver = new Logging\ConsoleLogger(2);
		$this->observers = array(
			"informational" => $infoObserver,
			"warning" => $warningObserver,
			"critical" => $criticalObserver
		);
		//logging manager
		$this->loggingManager = new Logging\LoggingManager();
		foreach ($this->observers as $observer) {
			$this->loggingManager->addObserver($observer);
		}
		//http client
		//needs an / at the end
		//TODO throw error if not or rebuild the uri
		if (is_null($url)) {
			$url = RwthMoodleApiLib\Config::$moodle_url;
		}
		$this->httpClient = new Http\Client($this->loggingManager, ["base_uri" => $url]);
		//rwth moodle api
		$this->rwthMoodleApiLib = new RwthMoodleApiLib\RwthMoodleApiLib($this->loggingManager, $this->httpClient);
	}

	/**
	 * Returns the LoggingManager that was injected into the RwthMoodleApiLib object
	 *
	 * @return Logging\LoggingManagerInterface
	 */
	public function getLoggingManager()
		: Logging\LoggingManagerInterface
	{
		return $this->loggingManager;
	}

	/**
	 * Returns the HttpClient that was injected into the RwthMoodleApiLib object
	 *
	 * @return Http\ClientInterface
	 */
	public function getHttpClient()
		: Http\ClientInterface
	{
		return $this->httpClient;
	}

	/**
	 * Returns the the RwthMoodleApiLib object
	 *
	 * @return RwthMoodleApiLibInterface
	 */
	public function getRwthMoodleApiLib()
		: RwthMoodleApiLib\RwthMoodleApiLibInterface
	{
		return $this->rwthMoodleApiLib;
	}

}
?>
