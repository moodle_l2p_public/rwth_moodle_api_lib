<?php
/**
 * The Moodle Webservice API Wrapper of the Rwth Aachen University
 *
 * PHP Version 7.0
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  RwthMoodleApiLib
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib;

use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Http as Http;

/**
 * RwthMoodleApiLib contains all methods that are needed by the
 * Moodle Webservice API Wrapper
 *
 * @category  RwthMoodleApiLib
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
interface RwthMoodleApiLibInterface
{

	/**
	 * Login to moodle webservice and get the Moodle token
	 *
	 * @param string $username   Moodle username
	 * @param string $password   Moodle password
	 * @param string $webservice Moodle webservice to login
	 *
	 * @return bool true if login was successful
	 */
	public function login(string $username, string $password, string $webservice)
		: bool;

	/**
	 * Login and get the Moodle token with the ext_token_login plugin
	 *
	 * @param string $oauthToken        The external OAuth token
	 * @param string $extTokenLoginUser The username of the ext_token_login dummy user
	 * @param string $webservice        The webservice to login
	 *
	 * @throws Exceptions\MoodleError
	 *
	 * @return bool true if login was successful
	 */
	public function extTokenLogin(string $oauthToken, string $extTokenLoginUser, string $webservice)
		: bool;

	/**
	 * Perform a rest call to the (already logged in) moodle webservice
	 *
	 * @param string $function The function name to execute on the moode webservice
	 * @param array  $params   Parameters
	 *
	 * @return Http\ResponseInterface The response from moodle
	 */
	public function restCall(string $function, array $params)
		: Http\ResponseInterface;
	/* public function restCall(string $function, array $params); */

	/**
	 * Get moodleToken.
	 *
	 * @return string moodleToken.
	 */
	public function getMoodleToken()
		: string;

	/**
	 * Set moodleToken.
	 *
	 * @param string $moodleToken the value to set.
	 *
	 * @return none
	 */
	public function setMoodleToken(string $moodleToken);

}
?>
