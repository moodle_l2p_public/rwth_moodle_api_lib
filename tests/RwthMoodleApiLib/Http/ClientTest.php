<?php
/**
 * The Moodle Webservice Api Wrapper of the Rwth Aachen University
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   PhpUnit
 * @package    RwthMoodleApiLib
 * @subpackage Curl
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

use PHPUnit\Framework\TestCase;
use RwthMoodleApiLib\Http as Http;
use RwthMoodleApiLib\Exceptions as Exceptions;
use RwthMoodleApiLib\Logging as Logging;

/**
 * Unit Tests for Curl HTTP
 *
 * @covers    RwthMoodleApiLib\Http\Client
 * @category  PhpUnit
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
final class ClientTest extends TestCase
{

	/**
	 * Create fixture
	 *
	 * @return none
	 **/
	protected function setUp()
	{
	}

	/**
	 * Test case for get function
	 *
	 * @return none
	 **/
	public function testGetBody()
	{
		$lman = new Logging\LoggingManager();
		$this->client = new Http\Client($lman, ["base_uri" => "http://google.de/"]);
		$this->assertNotEmpty($this->client->request("GET", "/")->getBody());
	}

	/**
	 * Test case for get function
	 *
	 * This try's to get from an invalid address
	 *
	 * @return none
	 **/
	public function testGetInvalidAdress()
	{
		$this->expectException(Exceptions\RequestException::class);
		$lman = new Logging\LoggingManager();
		$this->client = new Http\Client($lman, ["base_uri" => "127.0.0.256"]);
		$this->client->request("GET", "/");
	}

}

?>

