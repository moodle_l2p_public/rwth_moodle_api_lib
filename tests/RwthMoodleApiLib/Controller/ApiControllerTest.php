<?php
/**
 * The Moodle Webservice Api Wrapper of the Rwth Aachen University
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   PhpUnit
 * @package    RwthMoodleApiLib
 * @subpackage Controller
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Tests;

use PHPUnit\Framework\TestCase;
use RwthMoodleApiLib\Http as Http;
use RwthMoodleApiLib\Exceptions as Exceptions;
use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Injector as Injector;
use RwthMoodleApiLib\Controller as Controller;

/**
 * Unit Tests for ApiController
 *
 * @covers     RwthMoodleApiLib\Controller\ApiController
 * @category   PhpUnit
 * @package    RwthMoodleApiLib
 * @subpackage Controller
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
final class ApiControllerTest extends TestCase
{

	/**
	 * Create fixture
	 *
	 * @return none
	 **/
	protected function setUp()
	{
		$this->injector = new Injector\Injector();
		$this->apiCtrl = new Controller\ApiController($this->injector->getRwthMoodleApiLib());
	}

	/**
	 * Test case for get function
	 *
	 * @covers RwthMoodleApiLib\Controller\ApiController::getApi()
	 *
	 * @return none
	 **/
	public function testGetApi()
	{
		$expected = \RwthMoodleApiLib\RwthMoodleApiLibInterface::class;
		$actual = $this->apiCtrl->getApi();
		$this->assertInstanceOf($expected, $actual);
	}

}

?>

