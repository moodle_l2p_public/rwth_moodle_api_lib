<?php
/**
 * The Moodle Webservice Api Wrapper of the Rwth Aachen University
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   PhpUnit
 * @package    RwthMoodleApiLib
 * @subpackage Controller
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

namespace RwthMoodleApiLib\Tests;

use PHPUnit\Framework\TestCase;
use RwthMoodleApiLib\Http as Http;
use RwthMoodleApiLib\Exceptions as Exceptions;
use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Injector as Injector;
use RwthMoodleApiLib\Controller as Controller;

/**
 * Unit Tests for Announcement
 *
 * @covers     RwthMoodleApiLib\Controller\Announcement
 * @category   PhpUnit
 * @package    RwthMoodleApiLib
 * @subpackage Controller
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
final class AnnouncementTest extends TestCase
{

	private $injector;
	private $announcement;

	/**
	 * Create fixture
	 *
	 * @return none
	 **/
	protected function setUp()
	{
		$this->injector = new Injector\Injector();
		$this->announcement = new Controller\Announcement($this->injector->getRwthMoodleApiLib());
		/* $this->courseRoom->getApi()->login("test", "Geheim123!", "user_webservice"); */
		/* $this->courseRoom->getApi()->login("webservice", "Geheim123!", "webservice"); */
		/* $this->courseRoom->getApi()->setMoodleToken("7423b7c1d8ba48a57a58b725cc022768"); */
		$this->announcement->getApi()->extTokenLogin("webtoken", "ext_token_login_user", "webservice");
	}

	/**
	 * Test case for viewAllAnnouncements
	 *
	 * @covers RwthMoodleApiLib\Controller\Announcement::viewAllAnnouncements()
	 *
	 * @return none
	 **/
	public function testViewAllAnnouncements()
	{
		$announcement = $this->announcement->viewAllAnnouncements(2);
		$this->assertTrue(true);
	}

}

?>

