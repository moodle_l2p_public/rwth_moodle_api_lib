<?php
/**
 * The Moodle Webservice Api Wrapper of the Rwth Aachen University
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  PhpUnit
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

use PHPUnit\Framework\TestCase;
use RwthMoodleApiLib\RwthMoodleApiLib as RwthMoodleApiLib;
use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Http as Http;
use RwthMoodleApiLib\Exceptions as Exceptions;

/**
 * Unit Tests for RwthMoodleApiLib
 *
 * @covers    RwthMoodleApiLib\RwthMoodleApiLib
 * @category  PhpUnit
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
final class RwthMoodleApiLibTest extends TestCase
{

	protected $rwthMoodleApiLib;
	protected $loggingManager;
	protected $client;

	/**
	 * Create fixtures
	 *
	 * This method is called befor each unit test
	 *
	 * @return none
	 **/
	protected function setUp()
	{
	}

	/**
	 * Test Case for login method
	 *
	 * @covers RwthMoodleApiLib\RwthMoodleApiLib::login()
	 *
	 * @return none
	 **/
	public function testLogin()
	{
		$loggingManager = new Logging\LoggingManager();
		$response_mock = $this->createMock(Http\ResponseInterface::class);
		$response_mock->method('getBody')->willReturn('{"token":"1234567890abcdefghijklmnopqrstuv","privatetoken":null}');
		$client_mock = $this->createMock(Http\Client::class);
		$client_mock->method('request')->willReturn($response_mock);
		$rwthMoodleApiLib = new RwthMoodleApiLib($loggingManager, $client_mock);
		$this->assertTrue(empty($rwthMoodleApiLib->getMoodleToken()));
		$this->assertTrue($rwthMoodleApiLib->login("test", "test2", "test3"));
		$this->assertEquals("1234567890abcdefghijklmnopqrstuv", $rwthMoodleApiLib->getMoodleToken());
	}

	/**
	 * Test Case for extTokenLogin method
	 *
	 * @covers RwthMoodleApiLib\RwthMoodleApiLib::extTokenLogin()
	 *
	 * @return none
	 **/
	public function testExtTokenLogin()
	{
		$loggingManager = new Logging\LoggingManager();
		$response_mock = $this->createMock(Http\ResponseInterface::class);
		$response_mock->method('getBody')->willReturn('{"token":"1234567890abcdefghijklmnopqrstuv","privatetoken":null}');
		$client_mock = $this->createMock(Http\Client::class);
		$client_mock->method('request')->willReturn($response_mock);
		$rwthMoodleApiLib = new RwthMoodleApiLib($loggingManager, $client_mock);
		$this->assertTrue(empty($rwthMoodleApiLib->getMoodleToken()));
		$this->assertTrue($rwthMoodleApiLib->extTokenLogin("abcd", "ext_token_login_user", "user_webservice"));
		$this->assertEquals("1234567890abcdefghijklmnopqrstuv", $rwthMoodleApiLib->getMoodleToken());
	}

	/**
	 * Test for rest call
	 *
	 * @covers RwthMoodleApiLib\RwthMoodleApiLib::restCall()
	 *
	 * @return none
	 **/
	public function testRestCall()
	{
		$loggingManager = new Logging\LoggingManager();
		$response_mock = $this->createMock(Http\ResponseInterface::class);
		$return = '{"users":[{"id":2,"username":"admin","firstname":"Richard","lastname":"Stallman","fullname":"Richard Stallman","email":"stallman@fsfe.org","department":"","interests":"Gnu\/Linux, Freedom, GPL, FSF","firstaccess":1493379527,"lastaccess":1508332315,"auth":"manual","suspended":false,"confirmed":true,"lang":"en","theme":"","timezone":"99","mailformat":1,"description":"","descriptionformat":1,"url":"rms.sexy","profileimageurlsmall":"http:\/\/localhost\/moodle\/pluginfile.php\/5\/user\/icon\/boost\/f2?rev=158","profileimageurl":"http:\/\/localhost\/moodle\/pluginfile.php\/5\/user\/icon\/boost\/f1?rev=158"}],"warnings":[]}';
		$response_mock->method('getBody')->willReturn($return);
		$client_mock = $this->createMock(Http\Client::class);
		$client_mock->method('request')->willReturn($response_mock);
		$rwthMoodleApiLib = new RwthMoodleApiLib($loggingManager, $client_mock, "somevalidtoken");
		$response = $rwthMoodleApiLib->restCall("core_user_get_user", array());
		$this->assertEquals($return, $response->getBody());
	}

	/**
	 * Test Case for login method throwing an error
	 *
	 * @covers RwthMoodleApiLib\RwthMoodleApiLib::login()
	 *
	 * @return none
	 **/
	public function testLoginMoodleError()
	{
		$this->expectException(Exceptions\MoodleError::class);
		$loggingManager = new Logging\LoggingManager();
		$response_mock = $this->createMock(Http\ResponseInterface::class);
		$response_mock->method('getBody')->willReturn('{"error":"Ung\u00fcltige Anmeldedaten, bitte versuchen Sie es erneut!","errorcode":"invalidlogin","stacktrace":null,"debuginfo":null,"reproductionlink":null}');
		$client_mock = $this->createMock(Http\Client::class);
		$client_mock->method('request')->willReturn($response_mock);
		$rwthMoodleApiLib = new RwthMoodleApiLib($loggingManager, $client_mock);
		$rwthMoodleApiLib->login("test", "test2", "test3");
	}

	/**
	 * Test Case for extTokenLogin method throwing an error
	 *
	 * @covers RwthMoodleApiLib\RwthMoodleApiLib::extTokenLogin()
	 *
	 * @return none
	 **/
	public function testExtTokenLoginMoodleError()
	{
		$this->expectException(Exceptions\MoodleError::class);
		$loggingManager = new Logging\LoggingManager();
		$response_mock = $this->createMock(Http\ResponseInterface::class);
		$response_mock->method('getBody')->willReturn('{"error":"Ung\u00fcltige Anmeldedaten, bitte versuchen Sie es erneut!","errorcode":"invalidlogin","stacktrace":null,"debuginfo":null,"reproductionlink":null}');
		$client_mock = $this->createMock(Http\Client::class);
		$client_mock->method('request')->willReturn($response_mock);
		$rwthMoodleApiLib = new RwthMoodleApiLib($loggingManager, $client_mock);
		$this->assertTrue(empty($rwthMoodleApiLib->getMoodleToken()));
		$this->assertTrue($rwthMoodleApiLib->extTokenLogin("abcd", "ext_token_login_user", "user_webservice"));
		$this->assertEquals("1234567890abcdefghijklmnopqrstuv", $rwthMoodleApiLib->getMoodleToken());
	}

	/**
	 * Test for rest call throwing an error
	 *
	 * @covers RwthMoodleApiLib\RwthMoodleApiLib::restCall()
	 *
	 * @return none
	 **/
	public function testRestCallMoodleException()
	{
		$this->expectException(Exceptions\MoodleException::class);
		$loggingManager = new Logging\LoggingManager();
		$response_mock = $this->createMock(Http\ResponseInterface::class);
		$return = '{"exception":"dml_missing_record_exception","errorcode":"invalidrecord","message":"Datensatz kann nicht in der Datenbanktabelle external_functions gefunden werden"}';
		$response_mock->method('getBody')->willReturn($return);
		$client_mock = $this->createMock(Http\Client::class);
		$client_mock->method('request')->willReturn($response_mock);
		$rwthMoodleApiLib = new RwthMoodleApiLib($loggingManager, $client_mock, "somevalidtoken");
		$rwthMoodleApiLib->restCall("core_user_get_user", array());
	}

	/**
	 * Test for rest call throwing an error
	 *
	 * @covers RwthMoodleApiLib\RwthMoodleApiLib::restCall()
	 *
	 * @return none
	 **/
	public function testRestCallNotLoggedInException()
	{
		$this->expectException(Exceptions\NotLoggedInException::class);
		$loggingManager = new Logging\LoggingManager();
		$client_mock = $this->createMock(Http\Client::class);
		$rwthMoodleApiLib = new RwthMoodleApiLib($loggingManager, $client_mock);
		$rwthMoodleApiLib->restCall("core_user_get_user", array());
	}

}

?>
