<?php
/**
 * The Moodle Webservice Api Wrapper of the Rwth Aachen University
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   PhpUnit
 * @package    RwthMoodleApiLib
 * @subpackage Injector
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

use PHPUnit\Framework\TestCase;
use RwthMoodleApiLib\Http as Http;
use RwthMoodleApiLib\Exceptions as Exceptions;
use RwthMoodleApiLib\Logging as Logging;
use RwthMoodleApiLib\Injector as Injector;

/**
 * Unit Tests for Injector
 *
 * @covers     RwthMoodleApiLib\Injector\Injector
 * @category   PhpUnit
 * @package    RwthMoodleApiLib
 * @subpackage Injector
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
final class InjectorTest extends TestCase
{

	/**
	 * Create fixture
	 *
	 * @return none
	 **/
	protected function setUp()
	{
		$this->injector = new Injector\Injector();
	}

	/**
	 * Test case for get http client
	 *
	 * @covers RwthMoodleApiLib\Injector\Injector::getHttpClient()
	 *
	 * @return none
	 **/
	public function testGetHttpClient()
	{
		$expected = Http\Client::class;
		$actual = $this->injector->getHttpClient();
		$this->assertInstanceOf($expected, $actual);
	}

	/**
	 * Test case for get loggingManager
	 *
	 * @covers RwthMoodleApiLib\Injector\Injector::getLoggingManager()
	 *
	 * @return none
	 **/
	public function testGetLoggingManager()
	{
		$expected = Logging\LoggingManager::class;
		$actual = $this->injector->getLoggingManager();
		$this->assertInstanceOf($expected, $actual);
	}

	/**
	 * Test case for get rwth moodle api
	 *
	 * @covers RwthMoodleApiLib\Injector\Injector::getRwthMoodleApiLib()
	 *
	 * @return none
	 **/
	public function testGetRwthMoodleApiLib()
	{
		$expected = RwthMoodleApiLib\RwthMoodleApiLib::class;
		$actual = $this->injector->getRwthMoodleApiLib();
		$this->assertInstanceOf($expected, $actual);
	}

}

?>

