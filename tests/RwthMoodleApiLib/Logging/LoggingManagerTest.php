<?php
/**
 * The Moodle Webservice Api Wrapper of the Rwth Aachen University
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   PhpUnit
 * @package    RwthMoodleApiLib
 * @subpackage Logging
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

use PHPUnit\Framework\TestCase;
use RwthMoodleApiLib\Http as Http;
use RwthMoodleApiLib\Exceptions as Exceptions;
use RwthMoodleApiLib\Logging as Logging;

/**
 * Unit Tests for Curl HTTP
 *
 * @covers    RwthMoodleApiLib\Logging\LoggingManager
 * @category  PhpUnit
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
final class LoggingManagerTest extends TestCase
{

	/**
	 * Create fixture
	 *
	 * @return none
	 **/
	protected function setUp()
	{
		//TODO
		$this->loggingManager = new Logging\LoggingManager();
	}

	/**
	 * Test case for addObserver and getObservers
	 *
	 * @covers RwthMoodleApiLib\Logging\LoggingManager::addObserver()
	 * @covers RwthMoodleApiLib\Logging\LoggingManager::getObservers()
	 *
	 * @return none
	 **/
	public function testAddObserver()
	{
		$loggers = $this->loggingManager->getObservers();
		$this->assertEmpty($loggers);
		$this->logger_critical = new Logging\ConsoleLogger(2);
		$this->logger_warning = new Logging\ConsoleLogger(4);
		$this->logger_informational = new Logging\ConsoleLogger(6);
		$this->loggingManager->addObserver($this->logger_critical);
		$this->loggingManager->addObserver($this->logger_warning);
		$this->loggingManager->addObserver($this->logger_informational);
		$loggers = $this->loggingManager->getObservers();
		$this->assertContains($this->logger_critical, $loggers);
		$this->assertContains($this->logger_warning, $loggers);
		$this->assertContains($this->logger_informational, $loggers);
	}

	/**
	 * Test case for notifyObservers
	 *
	 * @covers RwthMoodleApiLib\Logging\LoggingManager::notifyObservers()
	 *
	 * @return none
	 **/
	public function testNotifyObservers()
	{
		$argument = "hallo welt";
		$this->logger_informational = new Logging\ConsoleLogger(6);
		$this->loggingManager->addObserver($this->logger_informational);
		$severity = 5;
		// expect the output once
		$this->expectOutputRegex("/.*$argument.*/");
		$this->loggingManager->notifyObservers($argument, $severity);
	}

	/**
	 * Test case for notifyObservers
	 *
	 * @covers RwthMoodleApiLib\Logging\LoggingManager::notifyObservers()
	 *
	 * @return none
	 **/
	public function testNotifyObservers2()
	{
		$argument = "hallo welt";
		$this->logger_warning = new Logging\ConsoleLogger(4);
		$this->loggingManager->addObserver($this->logger_warning);
		$severity = 5;
		// expect the output once
		$this->expectOutputString("");
		$this->loggingManager->notifyObservers($argument, $severity);
	}


	/**
	 * Test case for reportError
	 *
	 * @covers RwthMoodleApiLib\Logging\LoggingManager::reportError()
	 *
	 * @return none
	 **/
	public function testReportError()
	{
		$errMsg = "error not found";
		$errCode = 404;
		$severity = 3;
		$logger_warning = new Logging\ConsoleLogger(4);
		$this->loggingManager->addObserver($logger_warning);
		$this->expectOutputRegex("/.*$errCode.*/");
		$this->expectOutputRegex("/.*$errMsg.*/");
		$this->loggingManager->reportError($errCode, $errMsg, $this, $severity);
	}

	/**
	 * Test case for reportError
	 *
	 * @covers RwthMoodleApiLib\Logging\LoggingManager::reportError()
	 *
	 * @return none
	 **/
	public function testReportError2()
	{
		$errMsg = "error not found";
		$errCode = 404;
		$severity = 3;
		$logger_critical = new Logging\ConsoleLogger(2);
		$this->loggingManager->addObserver($logger_critical);
		$this->expectOutputString("");
		$this->loggingManager->reportError($errCode, $errMsg, $this, $severity);
	}


}

?>

