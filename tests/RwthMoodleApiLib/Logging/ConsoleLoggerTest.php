<?php
/**
 * The Moodle Webservice Api Wrapper of the Rwth Aachen University
 *
 * RwthMoodleApiLib
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category   PhpUnit
 * @package    RwthMoodleApiLib
 * @subpackage Logging
 * @author     Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/

use PHPUnit\Framework\TestCase;
use RwthMoodleApiLib\Http as Http;
use RwthMoodleApiLib\Exceptions as Exceptions;
use RwthMoodleApiLib\Logging as Logging;

/**
 * Unit Tests for Curl HTTP
 *
 * @covers    RwthMoodleApiLib\Logging\ConsoleLogger
 * @covers    RwthMoodleApiLib\Logging\Observer
 * @category  PhpUnit
 * @package   RwthMoodleApiLib
 * @author    Marco Schlicht <marcoschlicht@onlinehome.de>
 * @copyright 2017 Marco Schlicht PIT RWTH Aachen
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link      https://git.rwth-aachen.de/moodle/rwth_moodle_api
 **/
final class ConsoleLoggerTest extends TestCase
{

	/**
	 * Create fixture
	 *
	 * @return none
	 **/
	protected function setUp()
	{
	}

	/**
	 * Test case for getSeverity
	 *
	 * @covers RwthMoodleApiLib\Logging\Observer::getSeverityLvl()
	 *
	 * @return none
	 **/
	public function testGetSeverityLvl()
	{
		$logger = new Logging\ConsoleLogger(5);
		$this->assertEquals(5, $logger->getSeverityLvl());
		return $logger;
	}

	/**
	 * Test case for setSeverity
	 *
	 * @param ConsoleLogger $logger the console logger from testGetSeverity
	 *
	 * @depends testGetSeverityLvl
	 * @covers  RwthMoodleApiLib\Logging\Observer::setSeverityLvl()
	 *
	 * @return none
	 **/
	public function testSetSeverityLvl(Logging\ConsoleLogger $logger)
	{
		$logger->setSeverityLvl(4);
		$this->assertEquals(4, $logger->getSeverityLvl());
	}

	/**
	 * Test case for update function
	 *
	 * @covers RwthMoodleApiLib\Logging\ConsoleLogger::update()
	 *
	 * @return none
	 **/
	public function testUpdate()
	{
		$logger = new Logging\ConsoleLogger();
		$argument = "hallo welt";
		$this->expectOutputRegex("/.*$argument.*/");
		$logger->update($argument);
	}

	/**
	 * Test case for report error function
	 *
	 * @covers RwthMoodleApiLib\Logging\ConsoleLogger::reportError()
	 *
	 * @return none
	 **/
	public function testReport()
	{
		$logger = new Logging\ConsoleLogger();
		$errMsg = "error not found";
		$errCode = 404;
		$this->expectOutputRegex("/.*$errCode.*/");
		$this->expectOutputRegex("/.*$errMsg.*/");
		$logger->reportError($errCode, $errMsg, $this);
	}

}

?>

